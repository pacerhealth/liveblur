
Pod::Spec.new do |s|

  s.name         = "liveblur"
  s.version      = "0.0.1"
  s.summary      = "liveblur."
  s.description  = "liveblur.  "
  s.homepage     = "http://www.pacer.cc"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author             = { "Ray" => "guolei@me.com" }
  s.platform     = :ios
  s.source       = { :git => "http://git.oschina.net/lost/LiveBlur.git", :tag => "0.0.1" }

  s.source_files  = "*.{h,m}"

  s.frameworks   = "UIKit", "QuartzCore", "Accelerate"
  s.requires_arc = true

end
